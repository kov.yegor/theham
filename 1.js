let tabs = document.getElementsByClassName('tab');

let sections = document.getElementsByClassName('section');

for(let i =0; i<tabs.length; i++){
   tabs[i].onclick = tabclick
}

function tabclick(event){
  let tab = event.target;
  let tabId = tab.dataset.id;

  for(let k =0; k<tabs.length; k++){
    tabs[k].classList.remove('active');
    tabs[tabId-1].classList.add('active');

    sections[k].classList.remove('active');
    sections[tabId-1].classList.add('active'); 

  }
}


// $(function(){
//   $("div").slice(0, 12).show();
//   $("#").on('click', function(e){
//     e.preventDefault();
//     $("").slice(0,12)

//   })
// })  
function peopleSaySwitch(number) {
  let scrollFaces = document.querySelectorAll('.people_say_scroll_face');
  let scrollTexts = document.querySelectorAll('.people_say_text');

  scrollFaces.forEach(function(face){
      face.classList.remove('active');
      if(number == face.dataset.scrollFaceIndex) {
          face.classList.add('active');
      }
  });

  scrollTexts.forEach(function(text){
      text.classList.remove('active');
      if(number == text.dataset.scrollTextIndex) {
          text.classList.add('active'); 
      }
  });
}

let scrollButtons = document.querySelectorAll('.people_say_scroll_face');

scrollButtons.forEach(function(scrollButton){
  scrollButton.addEventListener('click', function(){
      peopleSaySwitch(this.dataset.scrollFaceIndex);
  });
});

let scrollArrows = document.querySelectorAll('.people_say_scroll .btn--scroll');

scrollArrows.forEach(function(scrollArrow){
  scrollArrow.addEventListener('click', function(){
      let position = document.querySelector('.people_say_scroll_face.active').dataset.scrollFaceIndex;
      if('left' == this.dataset.scrollDirection && 1 < position) {
          position--;
      }
      if('right' == this.dataset.scrollDirection && 4 > position) {
          position++;
      }
      peopleSaySwitch(position);
  });
});